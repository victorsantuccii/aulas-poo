namespace aulaFunction3
{
    function multiplicaPor( n: number): (x: number) => number 
    {
        return function(x: number): number{
            return x * n;
        }
    }
    let duplicar = multiplicaPor(2); // o 2 se associa ao N
    let resultado = duplicar(5); // o 5 se associa ao X
    console.log(resultado);
    
}