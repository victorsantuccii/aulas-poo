/*Crie um array com 6 números. Em seguida, use o método filter()
 para criar um novo array contendo apenas os números ímpares. */

 namespace filter_array
 {
    let numeros: number[] = [1, 2, 3, 4, 5];

    let impares = numeros.filter(function(number) 
    {
        return number % 2 != 0;
    });

    console.log(impares); 


 }