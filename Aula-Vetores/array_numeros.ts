/* Crie um array com 5 números.
 Em seguida, use um loop for para iterar sobre o array e exibir a soma de todos os números. */

 namespace numero_array
 {
    let numeros: number[] = [10,20,30,40,50];

    let somaNumeros: number = 0;
   
    for (let i = 0; i < numeros.length; i++)
    {
      // somaNumeros = somaNumeros + numeros[i]
       somaNumeros += numeros[i];
        
    }

    console.log(` A soma dos números é ${somaNumeros}`);

    // Criando uma iteração com multiplicação

    let multi: number = 1;
    for (let index = 0; index < numeros.length; index++) 
    {
      multi *= numeros[index]
      
      console.log(` O resultado da multiplicação é ${multi}`);
      
    }
   
        
 }