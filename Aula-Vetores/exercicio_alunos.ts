  /* Crie um vetor chamado " alunos " contendo três objetos, cada um representando um aluno com as seguintes propriedades: "nome ( string )"
    " idade " ( number ) e " notas "  ( array de números ). Preencha o vetor com informações fictícias.
    Em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno, calcule a média das notas e imprima o resultado na tela,
     juntamente com o nome e a idade do aluno. */


    interface Aluno {
        nome: string;
        idade: number;
        notas: number[];
    }


namespace exercicio_aluno{

    const alunos: Aluno[] = 
    [
        {nome: "João", idade: 14, notas:[8, 10, 6]},
        {nome: "Rafael", idade: 14, notas:[6, 5, 10]},
        {nome: "Roberto", idade: 15, notas:[10, 10, 2]},
    ]

    alunos.forEach((aluno) =>
    {  
        let media = aluno.notas.reduce((total , nota) =>  {return total + nota}) / aluno.notas.length
         if ( media >= 6)
         {
            console.log(`A média do aluno: ${aluno.nome} é igual ${media} e está aprovado`);
         }
         else{
            console.log(`A média do aluno: ${aluno.nome} é igual ${media} e está reprovado`); 
         }

        })
    }

    
    

