namespace media_array
{
    let notas: number[] = [10, 7, 6];
    let desc: string[] = ["Prova 1", "Prova 2", "Trabalho"];

    let qtdNotas: number = notas.length; // length para definir os 3 tipos de notas

    let somaNotas: number =  0; 

    for (let i = 0; i < notas.length; i++) 
    {
        somaNotas += notas[i]; //  somaNotas = somaNotas + notas[i];
    }

    let result: number;

    result = somaNotas / qtdNotas; // divisão das notas 

    for (let i = 0; i < notas.length; i++) 
    {
        console.log(`${desc[i]} = ${notas[i]}`) 
        
    }

    console.log(`A média final é: ${result}`);
    
    
}